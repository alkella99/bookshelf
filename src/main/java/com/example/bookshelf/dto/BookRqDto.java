package com.example.bookshelf.dto;

import lombok.Data;

@Data
public class BookRqDto {
    String name;
    String owner;
    String bookshelf;
}
