package com.example.bookshelf.dto;

import java.util.UUID;

public class BookRsDto {
    private UUID uuid;
    private String name;
    private String owner;
    private String bookshelf;
}
