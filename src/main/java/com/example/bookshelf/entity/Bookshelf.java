package com.example.bookshelf.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Table(name = "bookshelf")
public class Bookshelf {
    @Id
    private UUID uuid;

    private String name;

    @OneToMany
    private Set<Book> books;
}
