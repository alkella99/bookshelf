package com.example.bookshelf.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Getter
@Setter
@Entity
@Table(name = "books")
public class Book {
    @Id
    private UUID uuid;

    private String name;

    @ManyToOne
    private User owner;

    @ManyToOne
    private Bookshelf bookshelf;

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
