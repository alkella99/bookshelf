package com.example.bookshelf.controller;

import com.example.bookshelf.dto.BookRqDto;
import com.example.bookshelf.dto.BookRsDto;
import com.example.bookshelf.entity.Book;
import com.example.bookshelf.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BooksController {
    @Autowired
    private BookService bookService;

    @PostMapping("/save")
    public Book saveBook(@RequestBody BookRqDto bookRqDto){
        return bookService.saveBook(bookRqDto);
    }

    @GetMapping("/findbyname")
    public BookRsDto findByName(@RequestParam String name){
        return bookService.getBookByName(name);
    }

    @GetMapping("/test")
    public String checkSecurity(){
        return "String";
    }
}
