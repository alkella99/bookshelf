package com.example.bookshelf.service;

import com.example.bookshelf.dto.BookRqDto;
import com.example.bookshelf.dto.BookRsDto;
import com.example.bookshelf.entity.Book;
import com.example.bookshelf.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.UUID;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ModelMapper modelMapper;

    public Book getBook(UUID id) {
        return bookRepository.getById(id);
    }

    public Book saveBook(BookRqDto bookRqDto){
        Book book = new Book();
        book = modelMapper.map(bookRqDto, Book.class);
        book.setUuid(UUID.randomUUID());
        bookRepository.save(book);
        return book;
    }

    public BookRsDto getBookByName(String name) throws NotFoundException {
        Book book = new Book();
        book = bookRepository.findByName(name).orElseThrow(() -> new
                NotFoundException("Book not found"));
        return modelMapper.map(book, BookRsDto.class);
    }
}
