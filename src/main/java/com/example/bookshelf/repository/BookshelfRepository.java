package com.example.bookshelf.repository;

import com.example.bookshelf.entity.Bookshelf;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BookshelfRepository extends JpaRepository<Bookshelf, UUID> {
}
